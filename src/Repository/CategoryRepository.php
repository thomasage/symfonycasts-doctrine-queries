<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Category;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Category>
 */
final class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Category::class);
    }

    public function save(Category $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Category $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @return array<array{
     *     0: Category,
     *     fortuneCookieTotal: int,
     * }>
     */
    public function findAllOrdered(): array
    {
        $queryBuilder = $this->createQueryBuilder('category');

        return $this->addGroupByCategory($queryBuilder)
            ->orderBy('category.name')
            ->getQuery()
            ->getResult();
    }

    private function addGroupByCategory(QueryBuilder $queryBuilder = null): QueryBuilder
    {
        if (null === $queryBuilder) {
            $queryBuilder = $this->createQueryBuilder('category');
        }

        return $queryBuilder
            ->addSelect('COUNT( fortuneCookie.id ) AS fortuneCookiesTotal')
            ->leftJoin('category.fortuneCookies', 'fortuneCookie')
            ->addGroupBy('category.id');
    }

    /**
     * @return array<array{
     *     0: Category,
     *     fortuneCookieTotal: int,
     * }>
     */
    public function search(string $term): array
    {
        $termList = explode(' ', $term);
        $queryBuilder = $this->addOrderByCategoryName();

        return $this->addGroupByCategory($queryBuilder)
            ->andWhere(
                'category.name LIKE :searchTerm
                OR category.name IN ( :termList )
                OR category.iconKey LIKE :searchTerm
                OR fortuneCookie.fortune LIKE :searchTerm'
            )
            ->setParameter('searchTerm', '%'.$term.'%')
            ->setParameter('termList', $termList)
            ->getQuery()
            ->getResult();
    }

    private function addOrderByCategoryName(QueryBuilder $queryBuilder = null): QueryBuilder
    {
        if (null === $queryBuilder) {
            $queryBuilder = $this->createQueryBuilder('category');
        }

        return $queryBuilder->addOrderBy('category.name', Criteria::DESC);
    }

    public function findWithFortunesJoin(int $id): ?Category
    {
        return $this->addFortuneCookieJoinAndSelect()
            ->andWhere('category.id = :id')
            ->setParameter('id', $id)
            ->orderBy('RAND()', Criteria::ASC)
            ->getQuery()
            ->getOneOrNullResult();
    }

    private function addFortuneCookieJoinAndSelect(QueryBuilder $queryBuilder = null): QueryBuilder
    {
        if (null === $queryBuilder) {
            $queryBuilder = $this->createQueryBuilder('category');
        }

        return $queryBuilder
            ->addSelect('fortuneCookies')
            ->leftJoin('category.fortuneCookies', 'fortuneCookies');
    }
}
