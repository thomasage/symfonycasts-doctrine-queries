<?php

namespace App\Repository;

use App\Entity\Category;
use App\Entity\FortuneCookie;
use App\Model\CategoryFortuneStats;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<FortuneCookie>
 *
 * @method FortuneCookie|null find($id, $lockMode = null, $lockVersion = null)
 * @method FortuneCookie|null findOneBy(array $criteria, array $orderBy = null)
 * @method FortuneCookie[]    findAll()
 * @method FortuneCookie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FortuneCookieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FortuneCookie::class);
    }

    public function countNumberPrintedForCategory(Category $category): CategoryFortuneStats
    {
        //        return $this->createQueryBuilder('fortuneCookie')
        //            ->select(
        //                sprintf(
        //                    'NEW %s(
        //                        SUM( fortuneCookie.numberPrinted ),
        //                        AVG( fortuneCookie.numberPrinted ),
        //                        category.name
        //                    )',
        //                    CategoryFortuneStats::class
        //                )
        //            )
        //            ->innerJoin('fortuneCookie.category', 'category')
        //            ->where('fortuneCookie.category = :category')
        //            ->setParameter('category', $category)
        //            ->getQuery()
        //            ->getSingleResult();

        $connection = $this->getEntityManager()->getConnection();
        $sql = <<<'SQL'
SELECT SUM( fortune_cookie.number_printed ) AS fortunesPrinted,
       AVG( fortune_cookie.number_printed ) AS fortunesAverage,
       category.name AS categoryName
FROM fortune_cookie
INNER JOIN category ON fortune_cookie.category_id = category.id
WHERE fortune_cookie.category_id = :category
SQL;
        $statement = $connection->prepare($sql);
        $result = $statement->executeQuery([
            'category' => $category->getId(),
        ]);

        return new CategoryFortuneStats(...$result->fetchAssociative());
    }

    public function save(FortuneCookie $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(FortuneCookie $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public static function createFortuneCookiesStillInProductionCriteria(): Criteria
    {
        return Criteria::create()
            ->andWhere(Criteria::expr()->eq('discontinued', false));
    }
}
